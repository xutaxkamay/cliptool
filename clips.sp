#include <sourcemod>
#include <sdktools>

#pragma semicolon 1

#define PLUGIN_VERSION "1.0.0"
#define MAX_CONFIG_NAME 16 // TODO: 16 should be enough
#define MAX_CUSTOM_CFG 4 // TODO: increase once tested
// https://github.com/VSES/SourceEngine2007/blob/43a5c90a5ada1e69ca044595383be67f40b33c61/se2007/engine/baseclient.cpp#L1008
#define MAX_RENDER_LINES 64
#define MINIMUM_VALUE_COMPRESSED 0.1
#define UPDATE_INTERVAL 100

enum // Presets
{
	PRESET_CLIP,
	PRESET_MONSTERCLIP,
	PRESET_PLAYERCLIP,
	PRESET_MAX
};

enum // Colors
{
	COLOR_RED,
	COLOR_GREEN,
	COLOR_BLUE,
	COLOR_PINK,
	COLOR_MAX
};

int g_colors[COLOR_MAX][4] = {
	{255, 0, 0, 255},
	{0, 255, 0, 255},
	{0, 0, 255, 255},
	{255, 0, 255, 255}
};

// Rendering
enum struct vector_t
{
	float x;
	float y;
	float z;
};

enum struct line_t
{
	vector_t start;
	vector_t end;
};

enum struct renderline_t
{
	vector_t start;
	vector_t end;
	int color[4];
	int contents;
};

enum struct renderset_t
{
	ArrayList lines; // <line_t> this must be 4 bytes not yea but
	int contents;
};

ArrayList g_render_array; // <renderset_t>
ArrayList g_render_lines[MAXPLAYERS + 1];
int g_iBeamSprite;

// Render profile / config
enum struct rendercfg_t
{
	int contents; // bitflags
	int color[4]; // rgba
	char name[MAX_CONFIG_NAME]; // name of cfg
};

rendercfg_t g_presets[PRESET_MAX];

// Client settings
bool g_bEnablePreset[ MAXPLAYERS + 1 ][ PRESET_MAX ];
bool g_bEnableCustomCfg[ MAXPLAYERS + 1 ][ MAX_CUSTOM_CFG ];
rendercfg_t g_customcfg[ MAXPLAYERS + 1 ][ MAX_CUSTOM_CFG ];
int g_Index_Of_RenderedLine[MAXPLAYERS + 1];

// Database
Database g_db;

// Menus
Menu g_menu_main;
Menu g_menu_presets;

public Plugin myinfo =
{ 
	name = "Leafvis Renderer (cliptool)",
	author = "Kamay && ici",
	description = "Draws invisible brushes on the map",
	version = PLUGIN_VERSION,
	url = "https://gitlab.com/xutaxkamay/cliptool" 
};

/**
 * Clears and initialises g_render_array
 */
void initRenderArray()
{
	if (g_render_array != null)
	{
		// clean up nested arrays
		int size = g_render_array.Length;
		
		for (int i = 0; i < size; ++i)
		{
			renderset_t renderset;
			g_render_array.GetArray(i, renderset, sizeof(renderset));

			if (renderset.lines != null)
			{
				delete renderset.lines;
				renderset.lines = null;
			}
		}
		g_render_array.Clear();
		return;
	}
	g_render_array = new ArrayList( sizeof(renderset_t) );
}

/*
 * Returns the total number of lines
 */
int getTotalLines()
{
	int sum = 0;
	int size = g_render_array.Length;

	for (int i = 0; i < size; ++i)
	{
		renderset_t renderset;
		g_render_array.GetArray(i, renderset, sizeof(renderset));

		if (renderset.lines != null)
		{
			sum += renderset.lines.Length;
		}
	}
	return sum;
}

/**
 * Parses the .dat file output by the C++ cliptool plugin
 */
void parseRenderFile()
{
	File file;

	char path[ PLATFORM_MAX_PATH ] = "addons/cliptool/";
	char map[ 64 ];

	GetCurrentMap( map, sizeof( map ) );

	StrCat( path, sizeof( path ), map );
	StrCat( path, sizeof( path ), ".dat" );

	file = OpenFile( path, "rb", true, NULL_STRING );

	if ( file == null )
	{
		SetFailState( "Couldn't open \"%s\"", path );
		return;
	}

	initRenderArray();

	int size;
	file.ReadInt32( size );

	for (int i = 0; i < size; ++i)
	{
		int numlines, temp;
		file.ReadInt32( numlines );

		renderset_t renderset;
		renderset.lines = new ArrayList( sizeof(line_t) );

		for (int j = 0; j < numlines; ++j)
		{
			line_t line;

			file.ReadInt32( temp );
			line.start.x = view_as<float>( temp );
			file.ReadInt32( temp );
			line.start.y = view_as<float>( temp );
			file.ReadInt32( temp );
			line.start.z = view_as<float>( temp );

			file.ReadInt32( temp );
			line.end.x = view_as<float>( temp );
			file.ReadInt32( temp );
			line.end.y = view_as<float>( temp );
			file.ReadInt32( temp );
			line.end.z = view_as<float>( temp );

			renderset.lines.PushArray(line, sizeof(line_t));
		}

		file.ReadInt32( renderset.contents );

		g_render_array.PushArray( renderset, sizeof(renderset) );
	}

	LogMessage( "Parsed file successfully leafvis(%i), lines(%i)",
				GetArraySize(g_render_array),
				getTotalLines() );
	
	delete file;
}

/**
 * Creates the local sqlite db structure to hold client configs and connect to it
 */
void initDB()
{
	if (g_db != null)
	{
		delete g_db;
		g_db = null;
	}

	Database.Connect(callback_initDB, "cliptool");
}

void callback_initDB(Database db, const char[] error, any data)
{
	if (db == null)
	{
		SetFailState("[cliptool] Database connection failure: %s", error);
	}
	g_db = db;
	g_db.SetCharset("utf8");
	
	SQL_LockDatabase(g_db);
	SQL_FastQuery(g_db, "CREATE TABLE IF NOT EXISTS cliptool (id INT PRIMARY KEY, steam_id INT NOT NULL UNIQUE, contents INT NOT NULL, name CHAR(16) NOT NULL, red INT NOT NULL, green INT NOT NULL, blue INT NOT NULL, alpha INT NOT NULL);");
	SQL_UnlockDatabase(g_db);
}

/**
 * Creates the default render presets for common brush types
 */
void makePresets()
{
	// Clip
	g_presets[PRESET_CLIP].contents = CONTENTS_PLAYERCLIP | CONTENTS_MONSTERCLIP;
	array_copy( g_presets[PRESET_CLIP].color, g_colors[COLOR_RED], 4);
	strcopy(g_presets[PRESET_CLIP].name, MAX_CONFIG_NAME, "Clip");

	// Monsterclip
	g_presets[PRESET_MONSTERCLIP].contents = CONTENTS_MONSTERCLIP;
	array_copy( g_presets[PRESET_MONSTERCLIP].color, g_colors[COLOR_GREEN], 4);
	strcopy(g_presets[PRESET_MONSTERCLIP].name, MAX_CONFIG_NAME, "Monster Clip");

	// Playerclip
	g_presets[PRESET_PLAYERCLIP].contents = CONTENTS_PLAYERCLIP;
	array_copy( g_presets[PRESET_PLAYERCLIP].color, g_colors[COLOR_PINK], 4);
	strcopy(g_presets[PRESET_PLAYERCLIP].name, MAX_CONFIG_NAME, "Player Clip");
}

void updateLinesToRender(int client)
{
	g_Index_Of_RenderedLine[client] = 0;

	if (g_render_lines[client] != null)
	{
		delete g_render_lines[client];
		g_render_lines[client] = null;
	}

	g_render_lines[client] = new ArrayList( sizeof(renderline_t) );

	for (int preset = 0; preset < PRESET_MAX; preset++)
	{
		if (g_bEnablePreset[client][preset])
		{
			int contents = g_presets[preset].contents;

			for (int render = 0; render < g_render_array.Length; render++)
			{
				renderset_t renderset;
				g_render_array.GetArray(render, renderset, sizeof(renderset));

				if ((renderset.contents & contents) == contents)
				{
					for (int l = 0; l < renderset.lines.Length; l++)
					{
						line_t line;
						renderset.lines.GetArray(l, line, sizeof(line_t));

						renderline_t renderline;
						renderline.start.x = line.start.x;
						renderline.start.y = line.start.y;
						renderline.start.z = line.start.z;
						renderline.end.x = line.end.x;
						renderline.end.y = line.end.y;
						renderline.end.z = line.end.z;
						renderline.color[0] = g_presets[preset].color[0];
						renderline.color[1] = g_presets[preset].color[1];
						renderline.color[2] = g_presets[preset].color[2];
						renderline.color[3] = g_presets[preset].color[3];
						renderline.contents = renderset.contents;

						// replace overlapping lines which have fewer matching contents bits
						int overlap = findOverlappingLine( client, renderline );
						if ( overlap != -1 )
						{
							// replace
							g_render_lines[client].SetArray(overlap, renderline, sizeof(renderline_t));
						}
						else
						{
							g_render_lines[client].PushArray(renderline, sizeof(renderline_t));
						}
					}
				}
			}
		}
	}

	if ( !g_render_lines[client].Length )
	{
		// nothing to render
		delete g_render_lines[client];
		g_render_lines[client] = null;
	}
}

int findOverlappingLine(int client, const renderline_t renderline)
{
	if ( g_render_lines[client] == null )
	{
		return -1;
	}
	
	int size = g_render_lines[client].Length;
	for (int i = 0; i < size; ++i)
	{
		renderline_t against; // line to check against
		g_render_lines[client].GetArray(i, against, sizeof(against));

		// do positions match?
		if ( renderline.start.x == against.start.x &&
			 renderline.start.y == against.start.y &&
			 renderline.start.z == against.start.z &&
			 renderline.end.x == against.end.x &&
			 renderline.end.y == against.end.y &&
			 renderline.end.z == against.end.z )
		{
			// do any of the flags match
			if ( against.contents & renderline.contents )
			{
				// does the new one have more?
				if ( num_contents_set(against.contents) <= num_contents_set(renderline.contents) )
				{
					return i;
				}
			}
		}
	}
	return -1;
}

int num_contents_set(int contents)
{
	int sum = 0;

	for (int i = 0; i < 31; ++i)
	{
		if (contents & 1)
		{
			++sum;
		}
		contents = contents >> 1;
	}
	return sum;
}

public void OnPluginStart()
{
	// TODO: work on db and custom render cfg
	//initDB();
	//for(int i = 0; i <= MAXPLAYERS; i++)
	//	updateLinesToRender(i);

	makePresets();
	menu_main_create();
	menu_presets_create();

	PrintToServer("sizeof(vector_t) = %i", sizeof(vector_t));
	PrintToServer("sizeof(line_t) = %i", sizeof(line_t));
	PrintToServer("sizeof(renderset_t) = %i", sizeof(renderset_t));
	PrintToServer("sizeof(rendercfg_t) = %i", sizeof(rendercfg_t));

	RegConsoleCmd( "sm_clips", SM_Clips, "Command to dynamically toggle clip brushes visibility" );
	RegConsoleCmd( "sm_clipsmenu", SM_ClipsMenu, "Clips Menu" );
}

public void OnMapStart()
{
	parseRenderFile();

	// TODO: load a see-through walls sprite (.vmt with ignorez flag)
	g_iBeamSprite = PrecacheModel( "materials/sprites/laserbeam.vmt" );
}

public void OnClientPutInServer(int client)
{
	// TODO: add cookies later
	for(int i = 0; i < PRESET_MAX; i++)
		g_bEnablePreset[ client ][ i ] = false;
	
	updateLinesToRender( client );
}

public Action SM_Clips( int client, int args )
{
	// Can't use this cmd from within the server console
	if ( !client )
	{
		return Plugin_Handled;
	}

	g_bEnablePreset[ client ][ PRESET_PLAYERCLIP ] = !g_bEnablePreset[ client ][ PRESET_PLAYERCLIP ];

	updateLinesToRender( client );

	if ( g_bEnablePreset[ client ][ PRESET_PLAYERCLIP ] )
	{
		PrintToChat( client, "Showing clip brushes." );
	}
	else
	{
		PrintToChat( client, "Stopped showing clip brushes." );
	}

	return Plugin_Handled;
}

public Action SM_ClipsMenu( int client, int args )
{
	// Can't use this cmd from within the server console
	if ( !client )
	{
		return Plugin_Handled;
	}

	g_menu_main.Display(client, MENU_TIME_FOREVER);
	return Plugin_Handled;
}

void menu_main_create()
{
	if ( g_menu_main != null )
	{
		delete g_menu_main;
	}

	g_menu_main = new Menu(menu_main_callback);
	g_menu_main.SetTitle("Leafvis Renderer (cliptool)");
	g_menu_main.AddItem("presets", "Presets");
	g_menu_main.AddItem("customcfg", "Custom Profiles");
	g_menu_main.ExitButton = true;

	// TODO: revisit menu structure
}

public int menu_main_callback(Menu menu, MenuAction action, int client, int item)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[32];
			menu.GetItem(item, info, sizeof(info));

			if ( !strcmp(info, "presets") )
			{
				g_menu_presets.Display(client, MENU_TIME_FOREVER);
			}
			else if ( !strcmp(info, "customcfg") )
			{
				// TODO: send customcfg menu
			}
		}
	}
}

void menu_presets_create()
{
	if ( g_menu_presets != null )
	{
		delete g_menu_presets;
	}

	g_menu_presets = new Menu(menu_presets_callback, MENU_ACTIONS_DEFAULT | MenuAction_DisplayItem);
	g_menu_presets.SetTitle("Presets");

	char info[ 4 ];
	for ( int i = 0; i < PRESET_MAX; ++i )
	{
		IntToString( i, info, sizeof(info) );
		g_menu_presets.AddItem( info, g_presets[ i ].name );
	}
	
	g_menu_presets.ExitButton = true;
	g_menu_presets.ExitBackButton = true;
}

public int menu_presets_callback(Menu menu, MenuAction action, int client, int item)
{
	switch (action)
	{
		case MenuAction_Select:
		{
			char info[ 4 ];
			menu.GetItem(item, info, sizeof(info));

			// toggle preset
			int preset = StringToInt(info);
			g_bEnablePreset[ client ][ preset ] = !g_bEnablePreset[ client ][ preset ];
			g_menu_presets.Display(client, MENU_TIME_FOREVER);
			updateLinesToRender( client );
		}
		case MenuAction_DisplayItem:
		{
			// [ x ] : display the proper string /toggle
			char info[ 4 ], display[ MAX_CONFIG_NAME + 8 ];
			menu.GetItem(item, info, sizeof(info), _, display, sizeof(display));

			int preset = StringToInt(info);

			Format(display, sizeof(display), "[%s] - %s", 
				(g_bEnablePreset[ client ][ preset ] ? "x" : "  "),
				display);
			
			return RedrawMenuItem(display);
		}
		case MenuAction_Cancel:
		{
			if (item == MenuCancel_ExitBack)
			{
				g_menu_main.Display(client, MENU_TIME_FOREVER);
			}
		}
	}
	return 0;
}

public Action OnPlayerRunCmd(int client, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if ( IsFakeClient( client ) )
		return Plugin_Continue;

	if (g_render_lines[client] == null)
		return Plugin_Continue;

	// Render every 10 ticks.
	if (tickcount % UPDATE_INTERVAL)
		return Plugin_Continue;

	// Don't draw anything if there isn't anything to render..
	if (g_render_lines[client].Length)
	{
		// When is the next time that the beam at this index will be rendered 
		// = (Number of lines to render) / (Number of lines that will be rendered this tick) * tick_interval * 10
		// We multiply by two the tick interval so it doesn't blink.
		//float flLifeTime = ( ((float(MAX_RENDER_LINES) / g_render_lines[client].Length) / float(MAX_RENDER_LINES)) * (GetTickInterval() * UPDATE_INTERVAL) );

		// There is a problem with clamping values due to compression of netvars when its sent to client.
		// When it's lower than < MINIMUM_VALUE_COMPRESSED , the server sends the value as 0 and you get an infinite lifetime.
		// Good job valve.
		// TODO: Find proper minimal value
		//flLifeTime = (flLifeTime < MINIMUM_VALUE_COMPRESSED) ? MINIMUM_VALUE_COMPRESSED : flLifeTime + MINIMUM_VALUE_COMPRESSED;
		float flLifeTime = 1.0;

		// Have we rendered enough lines on this tick for this client?
		int renderedLines = 0;
		while (renderedLines < MAX_RENDER_LINES)
		{
			// Restart from index 0 for this client if we go above the limit of the lines that
			// Needs to be rendered.
			//if (g_Index_Of_RenderedLine[client] >= g_render_lines[client].Length)
			//{
			//	g_Index_Of_RenderedLine[client] = 0;
			//}

			renderline_t renderline;
			g_render_lines[client].GetArray(g_Index_Of_RenderedLine[client] % g_render_lines[client].Length, renderline, sizeof(renderline_t));

			int color_copy[4];
			float start_copy[3];
			float end_copy[3];

			array_copy(color_copy, renderline.color, 4);
			array_copy(start_copy, renderline.start, 3);
			array_copy(end_copy, renderline.end, 3);

			// TODO: We'll need to use the render_ignorez and render vmts depending on what the player chosen.
			TE_SetupBeamPoints(start_copy, end_copy, g_iBeamSprite, 0, 0, 0, flLifeTime, 0.5, 0.5, 0, 0.0, color_copy, 0);
			TE_SendToClient(client);

			// Increment the index for our next beam that needs to be drawn.
			++g_Index_Of_RenderedLine[client];

			// Increment the lines rendered
			renderedLines++;
		}
	}


	// Don't delete me it might be useful one day.
	/*int countContent = 0;

	for (int preset = 0; preset < PRESET_MAX; preset++)
	{		
		if (g_bEnablePreset[client][preset])
		{
			int contents = g_presets[preset].contents;

			for (int render = 0; render < g_render_array.Length; render++)
			{
				renderset_t renderset;
				g_render_array.GetArray(render, renderset, sizeof(renderset));
				
				if ((renderset.contents & contents) == contents)
				{
					countContent++;

					if(g_rendered_contents[client] >= countContent)
						continue;
					
					int old_rendered_lines = g_count_rendered_lines[client];

					bool bNeedsRemainder = false;

					while (g_count_rendered_lines[client] <= (old_rendered_lines + (MAX_RENDER_LINES - g_lines_remainder[client])))
					{
						if (g_count_rendered_lines[client] >= renderset.lines.Length)
						{
							g_rendered_contents[client]++;
							
							if (g_rendered_contents[client] >= g_maxCountContents[client])
							{
								g_rendered_contents[client] = 0;
							}

							g_lines_remainder[client] = (g_count_rendered_lines[client] % MAX_RENDER_LINES);
							g_count_rendered_lines[client] = 0;
							bNeedsRemainder = true;
							break;
						}

						line_t line;
						renderset.lines.GetArray(g_count_rendered_lines[client], line, sizeof(line));

						// thanks sm
						int color_copy[4];
						float start_copy[3];
						float end_copy[3];

						array_copy(color_copy, g_presets[preset].color, 4);
						array_copy(start_copy, line.start, 3);
						array_copy(end_copy, line.end, 3);

						TE_SetupBeamPoints(start_copy, end_copy, g_iBeamSprite, 0, 0, 0, flLifeTime, 0.5, 0.5, 0, 0.0, color_copy, 0);
						TE_SendToClient(client);

						g_count_rendered_lines[client]++;
					}
					
					if(!bNeedsRemainder)
					{
						g_lines_remainder[client] = 0;
					}
				}
			}
		}
	}*/

	return Plugin_Continue;
}

void array_copy(any[] dest, const any[] src, int size)
{
	for ( int i = 0; i < size; ++i )
	{
		dest[i] = src[i];
	}
}

/*
bool is_array_equal(const any[] arr1, const any[] arr2, int size)
{
	for (int i = 0; i < size; ++i)
	{
		if (arr1[i] != arr2[i])
		{
			return false;
		}
	}
	return true;
}

bool are_vectors_equal(const vector_t vec1, const vector_t vec2)
{
	return vec1.x == vec2.x && vec1.y == vec2.y && vec1.z == vec2.z;
}

void copy_vector(vector_t dest, const vector_t src)
{
	dest = src;
}

bool are_lines_equal(const line_t line1, const line_t line2)
{
	return are_vectors_equal(line1.start, line2.start) &&
		   are_vectors_equal(line1.end, line2.end);
}
*/
