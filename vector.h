#pragma once
#ifndef VECTOR
#	define VECTOR
#	include <float.h>
#	include <math.h>
#	include <stdlib.h>

#	define VectorSubtract( a, b, c ) \
		{                             \
			c[ 0 ] = a[ 0 ] - b[ 0 ]; \
			c[ 1 ] = a[ 1 ] - b[ 1 ]; \
			c[ 2 ] = a[ 2 ] - b[ 2 ]; \
		}
#	define VectorAdd( a, b, c )      \
		{                             \
			c[ 0 ] = a[ 0 ] + b[ 0 ]; \
			c[ 1 ] = a[ 1 ] + b[ 1 ]; \
			c[ 2 ] = a[ 2 ] + b[ 2 ]; \
		}
#	define VectorCopy( a, b ) \
		{                      \
			b[ 0 ] = a[ 0 ];   \
			b[ 1 ] = a[ 1 ];   \
			b[ 2 ] = a[ 2 ];   \
		}
#	define VectorScale( a, b, c ) \
		{                          \
			c[ 0 ] = b * a[ 0 ];   \
			c[ 1 ] = b * a[ 1 ];   \
			c[ 2 ] = b * a[ 2 ];   \
		}
#	define VectorClear( x )              \
		{                                 \
			x[ 0 ] = x[ 1 ] = x[ 2 ] = 0; \
		}
#	define VectorNegate( x ) \
		{                     \
			x[ 0 ] = -x[ 0 ]; \
			x[ 1 ] = -x[ 1 ]; \
			x[ 2 ] = -x[ 2 ]; \
		}

class Vector
{
 public:
	float x, y, z;

	Vector() { x = y = z = 0.f; }

	Vector( float ix, float iy, float iz )
	{
		x = ix;
		y = iy;
		z = iz;
	}

	void Init( float ix = 0.f, float iy = 0.f, float iz = 0.f )
	{
		x = ix;
		y = iy;
		z = iz;
	}

	float LengthSqr() const { return ( x * x + y * y + z * z ); }

	float Length2DSqr() const { return ( x * x + y * y ); }

	float Length() const { return sqrt( LengthSqr() ); }

	float Length2D() const { return sqrt( Length2DSqr() ); }

	float Empty( float tol = 0.01f ) const { return ( x > -tol && x < tol && y > -tol && y < tol && z > -tol && z < tol ); }

	void Zero() { x = y = z = 0.f; }

	Vector& operator=( const Vector& other )
	{
		x = other.x;
		y = other.y;
		z = other.z;
		return *this;
	}

	float& operator[]( int idx ) { return ( ( float* ) this )[ idx ]; }

	float operator[]( int i ) const { return reinterpret_cast< float* >( const_cast< Vector* >( this ) )[ i ]; }

	bool operator==( const Vector& other ) const { return ( other.x == x ) && ( other.y == y ) && ( other.z == z ); }

	bool operator!=( const Vector& other ) const { return ( other.x != x ) || ( other.y != y ) || ( other.z != z ); }

	Vector& operator+=( const Vector& other )
	{
		x += other.x;
		y += other.y;
		z += other.z;
		return *this;
	}

	Vector& operator-=( const Vector& other )
	{
		x -= other.x;
		y -= other.y;
		z -= other.z;
		return *this;
	}

	Vector& operator*=( float other )
	{
		x *= other;
		y *= other;
		z *= other;
		return *this;
	}

	Vector& operator*=( const Vector& other )
	{
		x *= other.x;
		y *= other.y;
		z *= other.z;
		return *this;
	}

	Vector& operator+=( float other )
	{
		x += other;
		y += other;
		z += other;
		return *this;
	}

	Vector& operator-=( float other )
	{
		x -= other;
		y -= other;
		z -= other;
		return *this;
	}

	Vector& operator/=( float other )
	{
		const float fucker = 1.f / other;
		x *= fucker;
		y *= fucker;
		z *= fucker;
		return *this;
	}

	Vector& operator/=( const Vector& other )
	{
		x /= other.x;
		y /= other.y;
		z /= other.z;
		return *this;
	}

	Vector operator+( const Vector& other ) const
	{
		Vector vec;
		vec.x = x + other.x;
		vec.y = y + other.y;
		vec.z = z + other.z;
		return vec;
	}

	Vector operator-( const Vector& other ) const
	{
		Vector vec;
		vec.x = x - other.x;
		vec.y = y - other.y;
		vec.z = z - other.z;
		return vec;
	}

	Vector operator*( float other ) const
	{
		Vector vec;
		vec.x = x * other;
		vec.y = y * other;
		vec.z = z * other;
		return vec;
	}

	Vector operator*( const Vector& other ) const
	{
		Vector vec;
		vec.x = x * other.x;
		vec.y = y * other.y;
		vec.z = z * other.z;
		return vec;
	}

	Vector operator/( float other ) const
	{
		Vector vec;
		vec.x = x / other;
		vec.y = y / other;
		vec.z = z / other;
		return vec;
	}

	Vector operator/( const Vector& other ) const
	{
		Vector vec;
		vec.x = x / other.x;
		vec.y = y / other.y;
		vec.z = z / other.z;
		return vec;
	}

	Vector operator-() const
	{
		Vector vec;
		vec.x = -x;
		vec.y = -y;
		vec.z = -z;
		return vec;
	}

	float Dot( const Vector& other ) const
	{
		const Vector vec = *this;
		return ( vec.x * other.x + vec.y * other.y + vec.z * other.z );
	}

	float Dot( const float* other ) const
	{
		const Vector vec = *this;
		return ( vec.x * other[ 0 ] + vec.y * other[ 1 ] + vec.z * other[ 2 ] );
	}

	void Normalize() { ( *this ) /= Length(); }

	Vector Normalized() const
	{
		Vector vec = *this;
		vec.Normalize();
		return vec;
	}

	float NormalizeInPlace()
	{
		Vector& v = *this;

		const float iradius = 1.f / ( this->Length() + FLT_EPSILON );

		v.x *= iradius;
		v.y *= iradius;
		v.z *= iradius;
		return iradius;
	}

	void MultAdd( const Vector& vOther, float fl )
	{
		x += fl * vOther.x;
		y += fl * vOther.y;
		z += fl * vOther.z;
	}
};

class VectorAligned : public Vector
{
 public:
	VectorAligned() {}

	VectorAligned( const Vector& vec )
	{
		this->x = vec.x;
		this->y = vec.y;
		this->z = vec.z;
		this->w = 0.f;
	}

	float w;
};

inline float DotProduct( const float* v1, const float* v2 ) { return v1[ 0 ] * v2[ 0 ] + v1[ 1 ] * v2[ 1 ] + v1[ 2 ] * v2[ 2 ]; }

inline float DotProduct( const Vector& a, const Vector& b ) { return ( a.x * b.x + a.y * b.y + a.z * b.z ); }
#endif