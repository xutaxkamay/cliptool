// clang-format off
#include "headers.h"
#include "bsp.h"
#include "main.h"
#include <cstring>
// clang-format on

//-----------------------------------------------------------------------------
// Global methods
//-----------------------------------------------------------------------------
static characterset_t s_BreakSet;
static bool s_bBuiltBreakSet = false;

void CharacterSetBuild( characterset_t* pSetBuffer, const char* pszSetString )
{
	int i = 0;

	// Test our pointers
	if ( !pSetBuffer || !pszSetString )
		return;

	memset( pSetBuffer->set, 0, sizeof( pSetBuffer->set ) );

	while ( pszSetString[ i ] )
	{
		pSetBuffer->set[ ( unsigned ) pszSetString[ i ] ] = 1;
		i++;
	}
}

//-----------------------------------------------------------------------------
// Tokenizer class
//-----------------------------------------------------------------------------
CCommand::CCommand()
{
	if ( !s_bBuiltBreakSet )
	{
		s_bBuiltBreakSet = true;
		CharacterSetBuild( &s_BreakSet, "{}()':" );
	}

	Reset();
}

CCommand::CCommand( int nArgC, const char** ppArgV )
{
	if ( !s_bBuiltBreakSet )
	{
		s_bBuiltBreakSet = true;
		CharacterSetBuild( &s_BreakSet, "{}()':" );
	}

	Reset();

	char* pBuf = m_pArgvBuffer;
	char* pSBuf = m_pArgSBuffer;
	m_nArgc = nArgC;
	for ( int i = 0; i < nArgC; ++i )
	{
		m_ppArgv[ i ] = pBuf;
		int nLen = strlen( ppArgV[ i ] );
		memcpy( pBuf, ppArgV[ i ], nLen + 1 );
		if ( i == 0 )
		{
			m_nArgv0Size = nLen;
		}
		pBuf += nLen + 1;

		bool bContainsSpace = strchr( ppArgV[ i ], ' ' ) != NULL;
		if ( bContainsSpace )
		{
			*pSBuf++ = '\"';
		}
		memcpy( pSBuf, ppArgV[ i ], nLen );
		pSBuf += nLen;
		if ( bContainsSpace )
		{
			*pSBuf++ = '\"';
		}

		if ( i != nArgC - 1 )
		{
			*pSBuf++ = ' ';
		}
	}
}

void CCommand::Reset()
{
	m_nArgc = 0;
	m_nArgv0Size = 0;
	m_pArgSBuffer[ 0 ] = 0;
}

characterset_t* CCommand::DefaultBreakSet() { return &s_BreakSet; }

//-----------------------------------------------------------------------------
// Helper function to parse arguments to commands.
//-----------------------------------------------------------------------------
const char* CCommand::FindArg( const char* pName ) const
{
	int nArgC = ArgC();
	for ( int i = 1; i < nArgC; i++ )
	{
		if ( !strcmp( Arg( i ), pName ) ) // changed from strcasecmp to strcmp
			return ( i + 1 ) < nArgC ? Arg( i + 1 ) : "";
	}
	return 0;
}

int CCommand::FindArgInt( const char* pName, int nDefaultVal ) const
{
	const char* pVal = FindArg( pName );
	if ( pVal )
		return atoi( pVal );
	else
		return nDefaultVal;
}
