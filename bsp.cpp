// clang-format off
#include "headers.h"
#include "bsp.h"
#include "main.h"
// clang-format on

const int MAX_LEAF_PVERTS = 128;

std::vector<leafvis_t> g_ClipVis;

static void AddPlaneToList(std::vector< cplane_t >& list, const Vector& normal, float dist, int invert)
{
	cplane_t plane;
	plane.dist = invert ? -dist : dist;
	plane.normal = invert ? -normal : normal;

	Vector point = plane.normal * plane.dist;
	for (std::size_t i = 0; i < list.size(); i++)
	{
		// same plane, remove or replace
		if (list[i].normal == plane.normal)
		{
			float d = DotProduct(point, list[i].normal) - list[i].dist;
			if (d > 0)
			{
				// new plane is in front of the old one
				list[i].dist = plane.dist;
			}
			// new plane is behind the old one
			return;
		}
	}

	list.push_back(plane);
}

Vector CSGInsidePoint(std::vector< cplane_t >& planeList)
{
	Vector point(0, 0, 0);

	for (std::size_t i = 0; i < planeList.size(); i++)
	{
		float d = DotProduct(planeList[i].normal, point) - planeList[i].dist;
		if (d < 0)
		{
			point -= planeList[i].normal * d;
		}
	}

	return point;
}

void TranslatePlaneList(std::vector< cplane_t >& planeList, const Vector& offset)
{
	for (std::size_t i = 0; i < planeList.size(); i++)
	{
		planeList[i].dist += DotProduct(offset, planeList[i].normal);
	}
}

void CSGPlaneList(leafvis_t* pVis, std::vector< cplane_t >& planeList)
{
	int planeCount = planeList.size();
	Vector vertsIn[MAX_LEAF_PVERTS], vertsOut[MAX_LEAF_PVERTS];

	// compute a point inside the volume defined by these planes
	Vector insidePoint = CSGInsidePoint(planeList);
	// move the planes so that the inside point is at the origin
	// NOTE: This is to maximize precision for the CSG operations
	TranslatePlaneList(planeList, -insidePoint);

	// Build the CSG solid of this leaf given that the planes in the list define a convex solid
	for (int i = 0; i < planeCount; i++)
	{
		// Build a big-ass poly in this plane
		int vertCount = PolyFromPlane(vertsIn, planeList[i].normal, planeList[i].dist, 9000.0f);  // BaseWindingForPlane()

		// Now chop it by every other plane
		int j;
		for (j = 0; j < planeCount; j++)
		{
			// don't clip planes with themselves
			if (i == j)
				continue;

			// Less than a poly left, something's wrong, don't bother with this polygon
			if (vertCount < 3)
				continue;

			// Chop the polygon against this plane
			vertCount = ClipPolyToPlane(vertsIn, vertCount, vertsOut, planeList[j].normal, planeList[j].dist, 0.1f);

			// Just copy the verts each time, don't bother swapping pointers (efficiency is not a goal here)
			for (int k = 0; k < vertCount; k++)
			{
				VectorCopy(vertsOut[k], vertsIn[k]);
			}
		}

		// We've got a polygon here
		if (vertCount >= 3)
		{
			// Copy polygon out
			pVis->polyVertCount.push_back(vertCount);
			for (j = 0; j < vertCount; j++)
			{
				// move the verts back by the initial translation
				Vector vert = vertsIn[j] + insidePoint;
				pVis->verts.push_back(vert);
			}
		}
	}
}

int FindMinBrush(CCollisionBSPData* pBSPData, int nodenum, int brushIndex)
{
	while (1)
	{
		if (nodenum < 0)
		{
			int leafIndex = -1 - nodenum;
			cleaf_t& leaf = pBSPData->map_leafs[leafIndex];
			int firstbrush = pBSPData->map_leafbrushes[leaf.firstleafbrush];
			if (firstbrush < brushIndex)
			{
				brushIndex = firstbrush;
			}
			return brushIndex;
		}

		cnode_t& node = pBSPData->map_rootnode[nodenum];
		brushIndex = FindMinBrush(pBSPData, node.children[0], brushIndex);
		nodenum = node.children[1];
	}

	return brushIndex;
}

void parseClipBrushes()
{
	g_ClipVis.clear();

	//g_ClipVis->color.Init(1.0f, 0.0f, 1.0f);

	CCollisionBSPData* pBSP = g_BSPData;

	int lastBrush = pBSP->numbrushes;

	if (pBSP->numcmodels > 1)
	{
		lastBrush = FindMinBrush(pBSP, pBSP->map_cmodels[1].headnode, lastBrush);
	}

	for (int i = 0; i < lastBrush; i++)
	{
		leafvis_t leafvis;
		cbrush_t* pBrush = &pBSP->map_brushes[i];

		std::vector< cplane_t > planeList;

		if (pBrush->IsBox())
		{
			cboxbrush_t* pBox = &pBSP->map_boxbrushes[pBrush->GetBox()];
			for (int i = 0; i < 3; i++)
			{
				Vector normal(0, 0, 0);
				normal[i] = 1.0f;

				AddPlaneToList(planeList, normal, pBox->maxs[i], true);
				AddPlaneToList(planeList, -normal, -pBox->mins[i], true);
			}
		}
		else
		{
			for (int j = 0; j < pBrush->numsides; j++)
			{
				cbrushside_t* pSide = &pBSP->map_brushsides[pBrush->firstbrushside + j];

				if (pSide->bBevel)
					continue;

				AddPlaneToList(planeList, pSide->plane->normal, pSide->plane->dist, true);
			}
		}

		CSGPlaneList(&leafvis, planeList);
		leafvis.contents = pBrush->contents;

		g_ClipVis.push_back(leafvis);
	}
}
