#include <cstddef>
#ifndef WIN32
#include <libgen.h>
#include <dlfcn.h>
#else
#include <Windows.h>
#endif
#include <float.h>
#include <iostream>
#ifndef WIN32
#include <link.h>
#endif
#include <string.h>
#include <string>
#ifndef WIN32
#include <sys/mman.h>
#endif
#include <sys/stat.h>
#include <sys/types.h>
#ifndef WIN32
#include <unistd.h>
#endif
#include <vector>
